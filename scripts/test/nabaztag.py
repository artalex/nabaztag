__author__ = 'artalex'

import usb.core
import usb.util
import time
import struct
import os

workpath = os.path.dirname(os.path.realpath(__file__))

# find our device
dev = usb.core.find(idVendor=0x03eb, idProduct=0x2423)

# was it found?
if dev is None:
    raise ValueError('Device not found')

# set the active configuration. With no arguments, the first
# configuration will be the active one
dev.set_configuration()

# get an endpoint instance
cfg = dev.get_active_configuration()
interface_number = cfg[(0, 0)].bInterfaceNumber
intf = usb.util.find_descriptor(cfg, bInterfaceNumber=interface_number, bAlternateSetting=True)
dev.set_interface_altsetting(intf, 1)

ep = usb.util.find_descriptor(intf, custom_match = lambda e: usb.util.endpoint_direction(e.bEndpointAddress) == usb.util.ENDPOINT_OUT and usb.util.endpoint_type(e.bmAttributes) == usb.util.ENDPOINT_TYPE_INTR)

assert ep is not None

# ENGINE 1
#cmd = struct.pack('<b', 0x40)
#cmd += struct.pack('<b', 0x00)
#cmd += struct.pack('<b', 2)
#ENGINE 2
#cmd += struct.pack('<b', 0x40)
#cmd += struct.pack('<b', 0x01)
#cmd += struct.pack('<b', -2)
# GREEN
cmd = struct.pack('<b', 0x60)
cmd += struct.pack('<b', 0x01)
cmd += struct.pack('<b', 100)
# RED
#cmd += struct.pack('<b', 0x60)
#cmd += struct.pack('<b', 0x02)
#cmd += struct.pack('<b', 100)
# SOUND
freq = int(16000)
cmd += struct.pack('<b', 0x10)
cmd += struct.pack('<h', freq)
ep.write(cmd)

# BLINK
#level = 1
#step = 1
#led = 1
#while True:
#    cmd = struct.pack('<b', 0x60)
#    cmd += struct.pack('<b', led)
#    cmd += struct.pack('<b', level)
#    ep.write(cmd)
#    time.sleep(0.005)
#    if level == 100 or level == 0:
#        step = -step
#        if level == 0:
#            led = 3 - led
#    level += step

# INPUT
# ep = usb.util.find_descriptor(intf, custom_match = lambda e: usb.util.endpoint_direction(e.bEndpointAddress) == usb.util.ENDPOINT_IN and usb.util.endpoint_type(e.bmAttributes) == usb.util.ENDPOINT_TYPE_INTR)
#
# # read the data
# while True:
#      print(ep.read(8, 5000))

ep = usb.util.find_descriptor(intf, custom_match = lambda e: usb.util.endpoint_direction(e.bEndpointAddress) == usb.util.ENDPOINT_OUT and usb.util.endpoint_type(e.bmAttributes) == usb.util.ENDPOINT_TYPE_BULK)

# SOUND
print('Start sound')
soundFile = open(os.path.join(workpath, 'countdown.bin'), 'rb')
dataToSend = soundFile.read()
print('Sound data size =', len(dataToSend))
soundFile.close()

start = 0
pause = 512.0 / freq
pause *= 0.9
print(pause)
while True:
    end = start + 1024
    ep.write(dataToSend[start : end])
    time.sleep(pause)
    start = start + 1024 if end < len(dataToSend) else 0

time.sleep(10)