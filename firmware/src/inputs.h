#include "asf.h"

#define INPUT_BUTTON 0
#define INPUT_VOLUME_PLUS 1
#define INPUT_VOLUME_MINUS 2

void inputs_init_hardware(void);
void inputs_check(void);
uint8_t inputs_get_status(uint8_t input);
