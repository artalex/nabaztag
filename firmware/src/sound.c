#include "asf.h"
#include "time.h"
#include "sound.h"
#include "inputs.h"
#include "usb.h"
#include "statistics.h"

#include <string.h>
#include <tc.h>
#include <dacc.h>

static uint16_t g_buffer[4096];
static volatile uint16_t g_buffer_head, g_buffer_tail, g_buffer_size;

static inline uint16_t get_continuous_data_size_irq(void)
{
	return g_buffer_head <= g_buffer_tail ? g_buffer_tail - g_buffer_head : ARRAYSIZE(g_buffer) - g_buffer_head;
}

static inline uint16_t get_buffer_capacity(void)
{
	return g_buffer_size;
}

static uint16_t copy_in_buffer(const uint16_t * data, uint16_t size)
{
	uint16_t tail = g_buffer_tail;
	
	// � ���� ����� �� ��������� ������� ��������
	uint32_t to_copy = min(size, ARRAYSIZE(g_buffer) - tail);
	memcpy((void *)(g_buffer + tail), (void *)data, (size_t)(to_copy << 1));
	
	if(to_copy < size)
	{
		// ����� ������ ����� ����������� � ������ ������
		memcpy(g_buffer, data + to_copy, (size - to_copy) << 1);
		tail = size - to_copy;
	}		
	else
	{
		// ��� ������ ������� ���������� � ����� ������
		tail += size;
	}
	
	return tail;
}

static inline void move_head_irq(uint16_t size)
{
	g_buffer_head += size;
	g_buffer_size += size;
	if(g_buffer_head >= ARRAYSIZE(g_buffer) && g_buffer_head > g_buffer_tail)
		g_buffer_head -= ARRAYSIZE(g_buffer);
}

static pdc_packet_t g_pdc_packet;
static volatile uint32_t g_pdc_packet_size;
static volatile uint8_t g_pdc_active;

static inline void disable_irq(void)
{
	dacc_disable_interrupt(DACC, DACC_IER_ENDTX);
}

static inline void restore_irq(void)
{
	if (g_pdc_active)
	dacc_enable_interrupt(DACC, DACC_IER_ENDTX);
}

#define DACC_PDC_PACKET_MAX_SIZE 512

void DACC_Handler(void)
{
	uint32_t status = dacc_get_interrupt_status(DACC);

	if (status & DACC_IER_ENDTX)
	{
#ifdef _DEBUG
		g_statistics.daccIrqQtty++;
#endif		
		
		move_head_irq(g_pdc_packet_size);
		uint16_t data_size = get_continuous_data_size_irq();

		if (data_size == 0)
		{
#ifdef _DEBUG
			g_statistics.daccEmptyQtty++;
#endif			
			g_pdc_packet_size = 0;
			g_pdc_active = 0;
			dacc_disable_interrupt(DACC, DACC_IDR_ENDTX);
		}
		else
		{
			g_pdc_packet.ul_addr = (uint32_t)(g_buffer + g_buffer_head);
			g_pdc_packet_size = min(DACC_PDC_PACKET_MAX_SIZE, data_size);
			g_pdc_packet.ul_size = g_pdc_packet_size;
			
			if (inputs_get_status(INPUT_VOLUME_MINUS))
			{
				// ��������� ���������
				for (uint16_t pos = g_buffer_head, end = g_buffer_head + g_pdc_packet_size; pos < end; pos++)
					g_buffer[pos] >>= 1;
			}
			else if (!inputs_get_status(INPUT_VOLUME_PLUS))
			{
				// ���� ��������
				memset(g_buffer + g_buffer_head, 0, data_size << 1);				
			}
			
			pdc_tx_init(PDC_DACC, &g_pdc_packet, NULL);
		}
	}
}

static void tc_adjust_frequency(uint16_t frequncy)
{
	#define FREQUENCY 8000
	#define DACC_CYCLES (FREQUENCY * 2)

	uint32_t divider = sysclk_get_cpu_hz() / frequncy;
	divider >>= 1;

	tc_write_ra(TC0, 0, divider);
	tc_write_rc(TC0, 0, divider + 1);
}

void sound_init_hardware(void)
{
	// Configure the PMC to enable the TC module.
	sysclk_enable_peripheral_clock(ID_TC0);

	// Init TC to waveform mode.
	tc_init(TC0, 0,
			TC_CMR_TCCLKS_TIMER_CLOCK1
			| TC_CMR_WAVE /* Waveform mode is enabled */
			| TC_CMR_ACPA_SET /* RA Compare Effect: set */
			| TC_CMR_ACPC_CLEAR /* RC Compare Effect: clear */
			| TC_CMR_CPCTRG /* UP mode with automatic trigger on RC Compare */
	);

	// Configure frequency
	tc_adjust_frequency(8000);

	// Start the timer counter
	tc_start(TC0, 0);

	// Enable clock for DACC
	sysclk_enable_peripheral_clock(ID_DACC);

	// Reset DACC registers
	dacc_reset(DACC);

	// Half word transfer mode
	dacc_set_transfer_mode(DACC, 0);

	/* Power save:
	 * sleep mode  - 0 (disabled)
	 * fast wakeup - 0 (disabled)
	 */
	dacc_set_power_save(DACC, 0, 0);

	// Use TC0 as trigger
	dacc_set_trigger(DACC, 1);

	// Disable TAG and select output channel DACC_CHANNEL
	dacc_set_channel_selection(DACC, 1);

	// Enable output channel DACC_CHANNEL
	dacc_enable_channel(DACC, 1);

	// Set up analog current
	dacc_set_analog_control(DACC, DACC_ACR_IBCTLCH0(0x02) | DACC_ACR_IBCTLCH1(0x02) | DACC_ACR_IBCTLDACCORE(0x01));
	
	NVIC_DisableIRQ(DACC_IRQn);
	NVIC_ClearPendingIRQ(DACC_IRQn);
	NVIC_EnableIRQ(DACC_IRQn);

	// Enable the PDC transmitter.
	pdc_enable_transfer(PDC_DACC, PERIPH_PTCR_TXTEN);
	
	g_pdc_packet_size = 0;
	g_pdc_active = 0;
}

void sound_start(uint16_t frequency)
{
	sound_stop();
	tc_adjust_frequency(frequency);
}	

void sound_stop(void)
{
	dacc_disable_interrupt(DACC, DACC_IER_ENDTX);

	g_pdc_packet_size = 0;
	g_buffer_head = g_buffer_tail = 0;
	g_buffer_size = ARRAYSIZE(g_buffer);
	g_pdc_active = 0;
}

uint8_t sound_data(const uint16_t * data, uint16_t size)
{
	uint16_t capacity = get_buffer_capacity();
	if (capacity < size)
		return 0;
		
	uint16_t tail = copy_in_buffer(data, size);
	disable_irq();
	g_buffer_tail = tail;
	g_buffer_size -= size;
	if (g_buffer_head >= ARRAYSIZE(g_buffer))
		g_buffer_head -= ARRAYSIZE(g_buffer);
	g_pdc_active = 1;
	restore_irq();
	
	return 1;
}
