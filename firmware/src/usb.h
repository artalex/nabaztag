#include "asf.h"

void usb_init(void);
uint8_t usb_check(void);

void usb_sound_start(void);
void usb_sound_stop(void);

/*! \brief Notify via user interface that enumeration is ok
 * This is called by vendor interface when USB Host enable it.
 *
 * \retval true if vendor startup is successfully done
 */
bool nabaztag_vendor_enable(void);

/*! \brief Notify via user interface that enumeration is disabled
 * This is called by vendor interface when USB Host disable it.
 */
void nabaztag_vendor_disable(void);

/*! \brief Attach or detach USB device
 * Called by UDC when Vbus line state changes
 *
 * \param b_high  True if VBus is present
 */
void nabaztag_vbus_action(bool b_high);

/*! \brief Manage the reception of setup request OUT
 *
 * \retval true if request accepted
 */
bool nabaztag_setup_out_received(void);

/*! \brief Manage the reception of setup request IN
 *
 * \retval true if request accepted
 */
bool nabaztag_setup_in_received(void);
