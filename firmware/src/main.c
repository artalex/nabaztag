#include <asf.h>

#include "time.h"
#include "inputs.h"
#include "engines.h"
#include "sound.h"
#include "leds.h"
#include "usb.h"
#include "statistics.h"

#define STRING_HEADER "-- Nabaztag firmware --\r\n" \
"-- "BOARD_NAME" --\r\n" \
"-- Compiled: "__DATE__" "__TIME__" --\r"

static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = 115200,
		.paritytype = UART_MR_PAR_NO
	};

	sysclk_enable_peripheral_clock(ID_UART);
	stdio_serial_init(UART, &uart_serial_options);
}

#ifdef _DEBUG
static uint32_t g_last_loop_timestamp;
static uint32_t g_loop_qtty;

static void init_mainloop_statistics(void)
{
	g_last_loop_timestamp = get_ticks();
	g_loop_qtty = 0;
	g_statistics.mainLoopMaxQtty = 0;
	g_statistics.mainLoopMinQtty = 0xFFFFFFFF;
}

static inline void update_mainloop_statistics(void)
{
	g_loop_qtty++;
	if (g_last_loop_timestamp != get_ticks())
	{
		g_statistics.mainLoopMinQtty = min(g_statistics.mainLoopMinQtty, g_loop_qtty);
		g_statistics.mainLoopMaxQtty = max(g_statistics.mainLoopMaxQtty, g_loop_qtty);
		g_last_loop_timestamp = get_ticks();
		g_loop_qtty = 0;
	}
}
#endif


int main (void)
{
    sysclk_init();
	board_init();
	configure_console();

    puts(STRING_HEADER);
	
#ifdef _DEBUG
	printf("CPU Frequency %li MHz\r\n", sysclk_get_cpu_hz() / 1000000);
#endif	
	
	time_init_hardware();
	inputs_init_hardware();
	engines_init_hardware();
	sound_init_hardware();
	leds_init_hardware();
	usb_init();

	puts("Work...\r");
	
#ifdef _DEBUG
	init_mainloop_statistics();
#endif

	while (1)
	{
		inputs_check();
		engines_check();
		
		if(!usb_check())
			break;
		
#ifdef _DEBUG
		update_mainloop_statistics();
#endif		
	}
	
	sound_stop();
	engines_stop();

	puts("\r\nExit\r");

#ifdef _DEBUG
	printf("\r\nStatistics:\r\n");
	
	printf("mainLoopMinQtty = %li\r\n", g_statistics.mainLoopMinQtty);
	printf("mainLoopMaxQtty = %li\r\n", g_statistics.mainLoopMaxQtty);

	printf("usartPdcBusyQtty = %li\r\n", g_statistics.usartPdcBusyQtty);
	printf("usartTimestampSkipQtty = %li\r\n", g_statistics.usartTimestampSkipQtty);
	printf("usartIrqQtty = %li\r\n", g_statistics.usartIrqQtty);

	printf("daccIrqQtty = %li\r\n", g_statistics.daccIrqQtty);
	printf("daccEmptyQtty = %li\r\n", g_statistics.daccEmptyQtty);
#endif
}
