/**
 * \file
 *
 * \brief USB configuration file
 *
 * Copyright (c) 2011-2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#ifndef _CONF_USB_H_
#define _CONF_USB_H_

#include "compiler.h"


#define UDD_NO_SLEEP_MGR

/**
 * USB Device Configuration
 * @{
 */

//! Device definition (mandatory)
#define  USB_DEVICE_VENDOR_ID             USB_VID_ATMEL
#define  USB_DEVICE_PRODUCT_ID            USB_PID_ATMEL_ASF_VENDOR_CLASS
#define  USB_DEVICE_MAJOR_VERSION         1
#define  USB_DEVICE_MINOR_VERSION         0
#define  USB_DEVICE_POWER                 0 // Consumption on Vbus line (mA)
#define  USB_DEVICE_ATTR                  USB_CONFIG_ATTR_SELF_POWERED

//! USB Device string definitions (Optional)
#define  USB_DEVICE_MANUFACTURE_NAME      "artalex"
#define  USB_DEVICE_PRODUCT_NAME          "nabaztag"

/**
 * Device speeds support
 * Low speed not supported by this vendor class
 * @{
 */
//! To authorize the High speed
#define  USB_DEVICE_HS_SUPPORT
//@}


/**
 * USB Device Callbacks definitions (Optional)
 * @{
 */
#define  UDC_VBUS_EVENT(b_vbus_high)      nabaztag_vbus_action(b_vbus_high)

//@}


/**
 * USB Interface Configuration
 * @{
 */

/**
 * Configuration of vendor interface
 * @{
 */
//! Interface callback definition
#define UDI_VENDOR_ENABLE_EXT()           nabaztag_vendor_enable()
#define UDI_VENDOR_DISABLE_EXT()          nabaztag_vendor_disable()
#define UDI_VENDOR_SETUP_OUT_RECEIVED()   nabaztag_setup_out_received()
#define UDI_VENDOR_SETUP_IN_RECEIVED()    nabaztag_setup_in_received()

//! endpoints size
#define UDI_VENDOR_EPS_SIZE_INT_IN    8
#define UDI_VENDOR_EPS_SIZE_INT_OUT   64
#define UDI_VENDOR_EPS_SIZE_BULK_IN   0
#define UDI_VENDOR_EPS_SIZE_BULK_OUT  1024
#define UDI_VENDOR_EPS_SIZE_ISO_IN    0
#define UDI_VENDOR_EPS_SIZE_ISO_OUT   0

//@}

//@}


/**
 * USB Device Driver Configuration
 * @{
 */
//! Limit the isochronous endpoint in singe bank mode for USBB driver
//! to avoid exceeding USB DPRAM.
#define UDD_ISOCHRONOUS_NB_BANK(ep) 1
//@}

//! The includes of classes and other headers must be done
//! at the end of this file to avoid compile error
#include "udi_vendor_conf.h"
#include "usb.h"

#endif // _CONF_USB_H_
