#include "asf.h"
#include "usb.h"
#include "sound.h"
#include "inputs.h"
#include "engines.h"
#include "leds.h"

#include "udc.h"

static volatile bool device_attached = false;
static volatile bool sound_acive = false;

static bool head_button_pressed = false;

static bool need_exit = false;

static uint16_t g_sound_buffer[512];
static volatile uint16_t g_sound_length = 0;

static uint8_t g_commands[64];
static volatile uint16_t g_commands_length = 0;

typedef struct usb_buttons_t
{
	uint8_t head_button;
	uint8_t reserved[7];
} usb_buttons;

static usb_buttons g_usb_buttons;

enum
{
	CMD_SoundStart = 0x10,
	CMD_SoundStop = 0x11,
	
	CMD_EngineState = 0x40,
	
	CMD_LedState = 0x60,
	
	CMD_Exit = 0xFF	
};

typedef struct sound_start_cmd_t
{
	uint8_t frequency[2];
		
} sound_start_cmd;

typedef struct engine_state_cmd_t
{
	uint8_t engine;
	int8_t speed;
	
} engine_state_cmd;

typedef struct led_state_cmd_t
{
	uint8_t led;
	uint8_t level;
	
} led_state_cmd;

void wait_for_sound_data(void);
void wait_for_commands(void);
void send_button_state(void);
void execute_commands(void);

void usb_bulk_out_received(udd_ep_status_t status, iram_size_t nb_transfered, udd_ep_id_t ep);
void usb_int_in_sent(udd_ep_status_t status, iram_size_t nb_transfered, udd_ep_id_t ep);
void usb_int_out_received(udd_ep_status_t status, iram_size_t nb_transfered, udd_ep_id_t ep);

void usb_init(void)
{
	head_button_pressed = inputs_get_status(INPUT_BUTTON);
	
	g_usb_buttons.head_button = 0;
	for (int8_t i = 0; i < ARRAYSIZE(g_usb_buttons.reserved); i++)
		g_usb_buttons.reserved[i] = i + 100;
	
	udc_start();
}

void usb_sound_start(void)
{
	if (!sound_acive)
	{
		sound_acive = true;
		if (device_attached)
			wait_for_sound_data();
	}		
}

void usb_sound_stop(void)
{
	sound_acive = false;	
}

uint8_t usb_check(void)
{
	if (device_attached)
	{
		// ��������� ��������� ������
		bool state = inputs_get_status(INPUT_BUTTON);
		if (state != head_button_pressed)
		{
			head_button_pressed = state;
			send_button_state();
		}
		
		// ����������� ����
		if (g_sound_length > 0)
		{
#ifdef _DEBUG
			printf("Put sound data: size = %i\r\n", (int)g_sound_length);
#endif
			sound_data(g_sound_buffer, g_sound_length);
			g_sound_length = 0;
		}
		
		// ��������� �������
		if (g_commands_length > 0)
		{
			execute_commands();
			g_commands_length = 0;
		}
	}
	
	return !need_exit;
}

void nabaztag_vbus_action(bool b_high)
{
	if (b_high)
	{
		// Attach USB Device
		udc_attach();
	}
	else
	{
		// VBUS not present
		udc_detach();
	}
}

bool nabaztag_vendor_enable(void)
{
	device_attached = true;
	
	if (sound_acive)
		wait_for_sound_data();
		
	wait_for_commands();
	send_button_state();
	
#ifdef _DEBUG
	puts("Device attached to USB\r");
#endif

	return true;
}

void nabaztag_vendor_disable(void)
{
	device_attached = false;
	
	sound_stop();
	engines_stop();
	leds_set_level(LED_GREEN, LED_MIN_LEVEL);
	leds_set_level(LED_RED, LED_MIN_LEVEL);

#ifdef _DEBUG
	puts("Device dettached from USB\r");
#endif
}

bool nabaztag_setup_out_received(void)
{
	return false;
}

bool nabaztag_setup_in_received(void)
{
	return false;
}

void wait_for_sound_data(void)
{
	if (sound_acive)
		udi_vendor_bulk_out_run((uint8_t *)g_sound_buffer, sizeof(g_sound_buffer), usb_bulk_out_received);
}

void wait_for_commands(void)
{
	udi_vendor_interrupt_out_run(g_commands, sizeof(g_commands), usb_int_out_received);
}

void send_button_state(void)
{
	g_usb_buttons.head_button = head_button_pressed;
	udi_vendor_interrupt_in_run((uint8_t *)&g_usb_buttons, sizeof(g_usb_buttons), usb_int_in_sent);
}

void execute_commands(void)
{
#ifdef _DEBUG
	printf("Received %i commands bytes\r\n", (int)g_commands_length);
#endif

	uint16_t length = g_commands_length;
	uint8_t * ptr = g_commands;
	uint8_t error = 0;
	
	while (!error && length > 0)
	{
		uint8_t code = *ptr;
		length--;
		ptr++;
		
		switch (code)
		{
			case CMD_SoundStart:
				if (length >= sizeof(sound_start_cmd))
				{
					sound_start_cmd * cmd = (sound_start_cmd *)ptr;
					length -= sizeof(sound_start_cmd);
					ptr += sizeof(sound_start_cmd);
					
					uint16_t frequency = (((uint16_t)cmd->frequency[1]) << 8) | cmd->frequency[0];
#ifdef _DEBUG
					printf("Sound start command: freqeuncy = %i\r\n", (int)frequency);
#endif
					if (frequency >= 8000 && frequency <= 44100)
					{
						sound_start(frequency);
						usb_sound_start();
						break;
					}
				}
#ifdef _DEBUG
				printf("Bad sound start command at %i\r\n", (int)(ptr - g_commands) - 1);
#endif
				error = 1;
				break;

			case CMD_SoundStop:
				usb_sound_stop();
				sound_stop();
				break;
		
			case CMD_EngineState:
				if (length >= sizeof(engine_state_cmd))
				{
					engine_state_cmd * cmd = (engine_state_cmd *)ptr;
					length -= sizeof(engine_state_cmd);
					ptr += sizeof(engine_state_cmd);
					
#ifdef _DEBUG
					printf("Engine state command: engine = %i, state = %i\r\n", (int)cmd->engine, (int)cmd->speed);
#endif
					if (cmd->engine < 2 && cmd->speed >= -9 && cmd->engine <= 9)
					{
						engines_set_speed(cmd->engine, cmd->speed);
						break;
					}
				}
#ifdef _DEBUG
				printf("Bad engine state command at %i\r\n", (int)(ptr - g_commands) - 1);
#endif
				error = 1;
				break;
		
			case CMD_LedState:
				if (length >= sizeof(led_state_cmd))
				{
					led_state_cmd * cmd = (led_state_cmd *)ptr;
					length -= sizeof(led_state_cmd);
					ptr += sizeof(led_state_cmd);
					
#ifdef _DEBUG
					printf("Led state command: led = %i, level = %i\r\n", (int)cmd->led, (int)cmd->level);
#endif
					if (cmd->level <= LED_MAX_LEVEL && (cmd->led == LED_GREEN || cmd->led == LED_RED))
					{
						leds_set_level(cmd->led, cmd->level);
						break;
					}
				}
#ifdef _DEBUG
				printf("Bad led state command at %i\r\n", (int)(ptr - g_commands) - 1);
#endif
				error = 1;
				break;
		
			case CMD_Exit:
				need_exit = true;
				break;
				
			default:
#ifdef _DEBUG
				printf("Unknown command %i at %i\r\n", (int)code, (int)(ptr - g_commands) - 1);
#endif			
				error = 1;
				break;
		}
	}
}

void usb_bulk_out_received(udd_ep_status_t status, iram_size_t nb_transfered, udd_ep_id_t ep)
{
	UNUSED(ep);
	if (sound_acive)
	{
		if (UDD_EP_TRANSFER_OK == status)
		{
#ifdef _DEBUG
			printf("Received sound data: size = %i\r\n", (int)nb_transfered);
#endif
			if (nb_transfered > 0 && !(nb_transfered & 0x01) && g_sound_length == 0)
				g_sound_length = nb_transfered / 2;
#ifdef _DEBUG
			else
				printf("Sound data ignored\r\n");
#endif
		}

		wait_for_sound_data();
	}
}

void usb_int_in_sent(udd_ep_status_t status, iram_size_t nb_transfered, udd_ep_id_t ep)
{
	UNUSED(status);
	UNUSED(nb_transfered);
	UNUSED(ep);
}

void usb_int_out_received(udd_ep_status_t status, iram_size_t nb_transfered, udd_ep_id_t ep)
{
	UNUSED(ep);
	if (UDD_EP_TRANSFER_OK == status)
	{
		if (nb_transfered > 0 && g_commands_length == 0)
			g_commands_length = nb_transfered;
	}

	wait_for_commands();
}
