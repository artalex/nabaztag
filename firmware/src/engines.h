#include "asf.h"

#define ENGINES_MAX_SPEED 9

void engines_init_hardware(void);

void engines_set_speed(uint32_t engine, int32_t speed);

void engines_stop(void);

void engines_check(void);
