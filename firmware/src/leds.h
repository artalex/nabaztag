#include "asf.h"

#define LED_MIN_LEVEL 0
#define LED_MAX_LEVEL 100

#define LED_GREEN 1
#define LED_RED 2

void leds_init_hardware(void);
void leds_set_level(uint8_t led, uint8_t level);
