#include "time.h"

volatile uint32_t g_ticks = 0;

void SysTick_Handler(void)
{
	g_ticks++;
}

inline uint32_t get_ticks(void)
{
	return g_ticks;
}

void time_init_hardware(void)
{
    if (SysTick_Config(sysclk_get_cpu_hz() / 1000))
	{
	    puts("-F- Systick configuration error\r\n");
	    while (1);
    }
}

void mdelay(uint32_t ticks)
{
	uint32_t cur_ticks = g_ticks;
	while ((g_ticks - cur_ticks) < ticks);
}

#define TIMER_FLAGS_ACTIVE 0x01
#define TIMER_FLAGS_AUTO 0x02

inline void timer_restart(timer_info * timer, uint32_t interval, uint8_t autorestart)
{
	timer->flags = TIMER_FLAGS_ACTIVE | (autorestart ? TIMER_FLAGS_AUTO : 0);
	timer->interval = interval;
	timer->timestamp = g_ticks;
}

inline void timer_stop(timer_info * timer)
{
	timer->flags = 0;
}

inline uint32_t timer_is_active(timer_info * timer)
{
	return timer->flags & TIMER_FLAGS_ACTIVE;
}

inline uint32_t timer_is_auto(timer_info * timer)
{
	return timer->flags & TIMER_FLAGS_AUTO;
}

inline uint32_t timer_is_over(timer_info * timer)
{
	uint32_t over = (timer->interval  && timer_get_time(timer) >= timer->interval) ? 1 : 0;
	if(over && timer_is_auto(timer))
		timer_restart(timer, timer->interval, 1);
	return over;
}

uint32_t timer_get_time(timer_info * timer)
{
	uint32_t time = 0;
	
	if (timer_is_active(timer))
	{
		uint32_t now = g_ticks;
		time = (now >= timer->timestamp) ? now - timer->timestamp : 0xFFFFFFFF - timer->timestamp + 1 + now;
	}
	
	return time;
}
