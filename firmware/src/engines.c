#include "asf.h"
#include "time.h"
#include "engines.h"
#include "statistics.h"

static volatile uint32_t g_pdc_transfer_complete = 1;

void USART0_Handler(void)
{
	uint32_t status = usart_get_status(USART0);
	if(status & US_CSR_ENDTX)
	{
		usart_disable_interrupt(USART0, US_IER_ENDTX);
		g_pdc_transfer_complete = 1;
		
#ifdef _DEBUG
		g_statistics.usartIrqQtty++;
#endif		
	}
}

void engines_init_hardware(void)
{
	gpio_configure_pin(PIN_USART0_TXD_IDX, PIN_USART0_TXD_FLAGS);

	const sam_usart_opt_t usart_console_settings = {
		115200,
		US_MR_CHRL_8_BIT,
		US_MR_PAR_NO,
		US_MR_NBSTOP_1_BIT,
		US_MR_CHMODE_NORMAL,
		0
	};

	/* Enable the peripheral clock in the PMC. */
	sysclk_enable_peripheral_clock(ID_USART0);

	/* Configure USART in serial mode. */
	usart_init_rs232(USART0, &usart_console_settings, sysclk_get_cpu_hz());

	/* Disable all the interrupts. */
	usart_disable_interrupt(USART0, 0xFFFFFFFF);

	/* Enable the transmitter. */
	usart_enable_tx(USART0);

	/* Configure and enable interrupt of USART. */
	NVIC_EnableIRQ(USART0_IRQn);

	/* Enable the PDC transmitter. */
    pdc_enable_transfer(PDC_USART0, PERIPH_PTCR_TXTEN);
	
	engines_stop();
}

struct engine_state_struct
{
	char dir;
	char speed;
};

typedef struct engine_state_struct engine_state;

static engine_state g_engine_states[2] = {
	{ .dir = 0, .speed = 0 }, { .dir = 0, .speed = 0 },
};

static char g_engine_data[4] = {0, 0, 0, '\r'};
static uint32_t g_last_send_timestamp = 0;
static pdc_packet_t g_pdc_packet;
static uint32_t g_need_send = 0;
static uint32_t g_next_engine = 0;

static void send_data(uint32_t engine)
{
    g_engine_data[0] = '1' + engine;
	g_engine_data[1] = g_engine_states[engine].dir;
	g_engine_data[2] = g_engine_states[engine].speed;
	
	g_pdc_transfer_complete = 0;
    g_pdc_packet.ul_addr = (uint32_t)g_engine_data;
    g_pdc_packet.ul_size = sizeof(g_engine_data);
    pdc_tx_init(PDC_USART0, &g_pdc_packet, NULL);
    usart_enable_interrupt(USART0, US_IER_ENDTX);
	
#ifdef _DEBUG
	printf("Send engine state: engine = %c, dir = %c, speed = %c, pause = %i\r\n", g_engine_data[0], g_engine_data[1], g_engine_data[2], (int)(get_ticks() - g_last_send_timestamp));
#endif

	g_last_send_timestamp = get_ticks();
}

void engines_set_speed(uint32_t engine, int32_t speed)
{
	char d = (speed >= 0 ? 'f' : 'r');
	char s = '0' + (speed > 0 ? speed : -speed);
	
	engine_state * state = g_engine_states + engine;
	if (state->dir != d || state->speed != s)
	{
		state->dir = d;
		state->speed = s;
		g_need_send |= (0x01 << engine);
	}
}

void engines_stop(void)
{
	engines_set_speed(0, 0);
	engines_set_speed(1, 0);
}

void engines_check(void)
{
	if (!g_need_send || !g_pdc_transfer_complete)
	{
#ifdef _DEBUG
		g_statistics.usartPdcBusyQtty += !g_pdc_transfer_complete;
#endif
	}		
		
	uint8_t skip = (get_ticks() - g_last_send_timestamp < 5);
#ifdef _DEBUG
	g_statistics.usartTimestampSkipQtty += skip;
#endif
	if (skip)
		return;
	
	for (uint32_t i = 0; i < ARRAYSIZE(g_engine_states); i++, g_next_engine++)
	{
		uint32_t index = g_next_engine % ARRAYSIZE(g_engine_states);
		if (g_need_send & (0x01 << index))
		{
			g_need_send &= ~(0x01 << index);
			send_data(index);
			break;
		}
	}
}
