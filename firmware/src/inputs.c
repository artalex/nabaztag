#include "asf.h"
#include "inputs.h"

struct pin_info_struct
{
	uint8_t index;
};

typedef struct pin_info_struct pin_info;

static pin_info g_pins[] = {
	{ .index = 13 }, // Button
	
	{ .index = 14 }, // Volume +
	{ .index = 15 }, // Volume -
};

#define PINS_MASK 0x0000E000

static volatile uint32_t g_status;

void inputs_init_hardware(void)
{
	sysclk_enable_peripheral_clock(ID_PIOC);
	gpio_configure_group(PIOC, PINS_MASK, PIO_INPUT | PIO_PULLUP | PIO_DEBOUNCE);
	
	g_status = ((Pio *)PIOC)->PIO_PDSR & PINS_MASK;
}

void inputs_check(void)
{
	g_status = ((Pio *)PIOC)->PIO_PDSR & PINS_MASK;
}

uint8_t inputs_get_status(uint8_t input)
{
	return !(g_status & (0x01ul << g_pins[input].index));
}
