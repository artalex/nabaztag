#include "asf.h"

uint32_t get_ticks(void);

void time_init_hardware(void);

void mdelay(uint32_t ticks);

struct timer_struct
{
	uint32_t flags;
	uint32_t timestamp;
	uint32_t interval;
};

typedef struct timer_struct timer_info;

void timer_restart(timer_info * timer, uint32_t interval, uint8_t autorestart);
void timer_stop(timer_info * timer);
uint32_t timer_is_active(timer_info * timer);
uint32_t timer_is_auto(timer_info * timer);
uint32_t timer_is_over(timer_info * timer);
uint32_t timer_get_time(timer_info * timer);
