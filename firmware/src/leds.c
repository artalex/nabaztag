#include "asf.h"
#include "leds.h"

/** PWM frequency in Hz */
#define PWM_FREQUENCY      1000
/** Period value of PWM output waveform */
#define PERIOD_VALUE       100

/** PWM channel instance for LEDs */
pwm_channel_t g_pwm_channel_led;

void leds_init_hardware(void)
{
    pmc_enable_periph_clk(ID_PWM);
	sysclk_enable_peripheral_clock(ID_PIOC);

    /* Configure PWM pin */
    gpio_configure_pin(PIO_PC21_IDX, PIO_PERIPH_B | PIO_DEFAULT);
    gpio_configure_pin(PIO_PC22_IDX, PIO_PERIPH_B | PIO_DEFAULT);

    /* Set PWM clock A as PWM_FREQUENCY * PERIOD_VALUE (clock B is not used) */
    pwm_clock_t clock_setting = {
	    .ul_clka = PWM_FREQUENCY * PERIOD_VALUE,
	    .ul_clkb = 0,
	    .ul_mck = sysclk_get_cpu_hz()
    };
    pwm_init(PWM, &clock_setting);

    /* Initialize PWM channel */
    g_pwm_channel_led.ul_prescaler = PWM_CMR_CPRE_CLKA;  /* Use PWM clock A as source clock */
    g_pwm_channel_led.ul_period = PERIOD_VALUE;  /* Period value of output waveform */
    g_pwm_channel_led.ul_duty = 0;  /* Duty cycle value of output waveform */
    g_pwm_channel_led.channel = PWM_CHANNEL_4;
    pwm_channel_init(PWM, &g_pwm_channel_led);

    /* Initialize PWM channel */
    g_pwm_channel_led.channel = PWM_CHANNEL_5;
    pwm_channel_init(PWM, &g_pwm_channel_led);

    /* Enable PWM channels for LEDs */
    pwm_channel_enable(PWM, PWM_CHANNEL_4);
    pwm_channel_enable(PWM, PWM_CHANNEL_5);
}

void leds_set_level(uint8_t led, uint8_t level)
{
	g_pwm_channel_led.channel = (led == LED_GREEN ? PWM_CHANNEL_4 : PWM_CHANNEL_5);
	pwm_channel_update_duty(PWM, &g_pwm_channel_led, level);
}
