#include "asf.h"

#ifdef _DEBUG

struct statistics_struct
{
	uint32_t mainLoopMinQtty;	
	uint32_t mainLoopMaxQtty;
	
	uint32_t usartPdcBusyQtty;
	uint32_t usartTimestampSkipQtty;
	uint32_t usartIrqQtty;

	uint32_t daccIrqQtty;
	uint32_t daccEmptyQtty;
};

typedef struct statistics_struct statistics_data;

extern statistics_data g_statistics;

#else

#endif